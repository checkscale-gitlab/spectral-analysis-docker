A docker container for doing spectral analysis in R.

# Get Docker
This script is provided as a Docker container. You will need to install Docker on your machine to run it.
* Get Docker at: https://www.docker.com/get-started
* get `docker-compose`at: https://docs.docker.com/compose/install/

# Example
1. Clone the repo
2. `docker-compose up -d` will download the ready container from docker hub
3. `make example` will take the audio file in `./example/example.mp3` and draw various visualizations in `./example/*.png`.

(BTW: the example audio is a goshawk call).

# Recipes
Here are some recipes for visualizing a sound file. 
More stuff you can do with seewave: http://rug.mnhn.fr/seewave/

1. Start the container `docker-compose up -d`

If you have an mp3 file (e.g. example/example.mp3), you will have to convert it first:

2. Copy the xxx.mp3 file into the container `docker cp xxx.mp3 spectral:/workspace/`
3. connect to the container `docker exec -ti spectral bash`
4. convert the file to wav `sox xxx.mp3 -r 22050 -c 1 xxx.wav`

5. From inside  the container, plot some graphics:

## Spectrogram
![Spectrogram](https://gitlab.com/alvarosaurus/spectral-analysis-docker/uploads/93b130a304002a3ae9639f54f8829887/example_spc.png)

`Rscript /src/plotSpectrogram.R xxx.wav xxx_spc.png`

The spectrogram is now in ./workspace/xxx_spc.png on the host.

## Oscillogram
![Oscillogram](https://gitlab.com/alvarosaurus/spectral-analysis-docker/uploads/5f42597a598531d7fb2a717b48d45168/example_osc.png)

`Rscript /src/plotOscillogram.R xxx.wav xxx_osc.png`

The oscillogram is now in ./workspace/xxx_osc.png on the host.

## Spectral analysis
![Spectral analysis](https://gitlab.com/alvarosaurus/spectral-analysis-docker/uploads/3b8e0cead6ab0fc9b9c9cda714b11f5a/example_spca.png)

`Rscript /src/plotSpectral.R xxx.wav xxx_spca.png`

The spectrum is now in ./workspace/xxx_spca.png on the host.

## Color-coded spectrogram
![Color-coded spectrogram](https://gitlab.com/alvarosaurus/spectral-analysis-docker/uploads/2adf5396748dd35646850a2ac5800ca3/example_spcolor.png)

`Rscript /src/plotSpectrogramColor.R xxx.wav xxx_spcolor.png`

The color-coded spectrogram is now in ./workspace/xxx_spcolor.png on the host.

# Limitations
This container cannot do 3D graphics.

# References
Sueur J, Aubin T, Simonis C (2008) Seewave: a free modular tool for sound analysis and synthesis. Bioacoustics 18: 213-226.